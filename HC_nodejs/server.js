'use strict';
var express = require('express');
var app = express();
var port = process.env.PORT || 80;
var path = require('path');


var Highcharts = require('highcharts');
/*
// Load module after Highcharts is loaded
require('highcharts/modules/exporting')(Highcharts);
*/
// Create the chart
Highcharts.chart('container', { });



app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(port, '127.0.0.1', function () {
    console.log('HTTP server running on http://127.0.0.1/');
});
